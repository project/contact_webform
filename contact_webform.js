/**
 * Ajax Forms plugin for contact_webform
 * 
 */

Drupal.Ajax.plugins.contactformplus = function(hook, args) {   
  if (hook != 'afterComplete') {
    return true;
  }
  
  // check for webform error message, don't clear form if any error message is printed
  // Change selectors to match to your theme markup
  if ($(".node .ajax-form .status").length == 0) {    
    return true;
  }  
  $(".node form.ajax-form").each(function() {    
    $(this).get(0).reset();
  });  
  //$(".webform form .status").fadeOut(2500, "linear", complete);
  return true;
}

